const fs  = require('fs');
const path = require('path');
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: {
    app: './src/index.ts',
  },
  output: {
    filename: '[name].js',
  },
  resolve: {
    extensions: ['.ts', '.js'],
    modules: [
      path.resolve(__dirname,'./node_modules'),
      path.resolve(__dirname, '.'),
    ],
  },
  module: {
    rules: [
      { 
        test: /\.ts?$/, 
        loader: "ts-loader",
        exclude: /node_modules/      
      },
      {
        test: /\.yaml$/,
        use: ['json-loader', 'yaml-loader'],
      },
      {
        test: /\.(ejs|html)$/,
        loader: 'html-loader',
      },
    ],
  },
  node: {
    __dirname: false,
  },  
  target: 'node',
};
