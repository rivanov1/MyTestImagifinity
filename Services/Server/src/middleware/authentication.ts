import * as passport from 'passport';

export const authentication = passport.authenticate('jwt', { session: false });
