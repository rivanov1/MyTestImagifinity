import * as expressWinston from 'express-winston';
import { config } from 'src/app-config';
import * as winston from 'winston';
import 'winston-mongodb';

export const httpLogger = expressWinston.logger({
  colorize: false,
  expressFormat: true,
  ignoreRoute: () => false,
  meta: true,
  msg: 'HTTP {{req.method}} {{req.url}}',
  transports: [
    new winston.transports.Console({
      colorize: true,
      json: true,
    }),
    new winston.transports.MongoDB({
      collection: 'logs',
      db: config.mongo.url,
    }),
  ],
});
