import * as express from 'express';

export const errorHandler = (
  err: Error,
  _req: express.Request,
  res: express.Response,
  _next: express.NextFunction,
) => {
  res.status(500).send(err.message);
};
