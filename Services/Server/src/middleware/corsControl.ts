import * as cors from 'cors';

export const corsControl = cors({
  allowedHeaders: 'set-cookie,Content-Type,Authorization,Content-Length,X-Requested-With',
  credentials: true,
  exposedHeaders: 'set-cookie,Content-Type,Authorization,Content-Length,X-Requested-With',
  origin: 'http://localhost:8000',
});
