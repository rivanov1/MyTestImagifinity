import * as expressSession from 'express-session';

export const sessionControl = expressSession({
  resave: false,
  saveUninitialized: false,
  secret: 'RruHHLpVBL',
});
