import { ObjectId } from 'mongodb';

export interface BinEvent {
  binId: ObjectId;
  description?: string;
  receivedAt: Date;
  type: BinEventType;
}

type BinEventType = 'alarm' | 'transmission';
