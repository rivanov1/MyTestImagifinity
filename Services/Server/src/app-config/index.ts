import * as fs from 'fs';
import * as joi from 'joi-browser';

interface Config {
  mongo: {
    url: string;
    dbName: string;
  };
  tokenExpiration: number;
  appURL: string;
  swaggerURL: string;
  core: {
    swagger: string;
    port: number;
    swaggerPort: number;
    protocol: string;
    backgroundJobInterval: number;
  };
  devices: {
    swagger: string;
    port: number;
    swaggerPort: number;
    protocol: string;
  };
}

const schema = {
  appURL: joi.string().required(),
  core: joi
    .object()
    .keys({
      backgroundJobInterval: joi.number(),
      port: joi
        .number()
        .integer()
        .required(),
      protocol: joi.string().required(),
      swaggerPort: joi
        .number()
        .integer()
        .required(),
    })
    .required(),
  devices: joi
    .object()
    .keys({
      port: joi
        .number()
        .integer()
        .required(),
      protocol: joi.string().required(),
      swaggerPort: joi
        .number()
        .integer()
        .required(),
    })
    .required(),
  mongo: {
    dbName: joi.string().required(),
    url: joi.string().required(),
  },
  swaggerURL: joi.string().required(),
  tokenExpiration: joi
    .number()
    .integer()
    .required(),
};

export let config: Config;

export function init() {
  config = JSON.parse(fs.readFileSync(`${__dirname}/config.json`, 'utf8'));
  joi.validate(config, schema, (err: Error): void => {
    if (err) {
      throw err;
    }
  });
}
