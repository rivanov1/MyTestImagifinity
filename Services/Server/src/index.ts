import 'express-async-errors';

import 'src/startup/init-config';

import 'src/startup/connect-database';

import 'src/startup/init-authentication';

// import 'src/startup/configure-http-logger';

import 'src/startup/start-apps';

// import 'src/startup/start-tasks';
