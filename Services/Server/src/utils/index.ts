import { ObjectID } from 'bson';
import { Response } from 'express';
import { db } from 'src/mongo';

const readCollection = async <T>(name: string, criteria = { filter: {}, fields: {} }) => {
  return db
    .collection<T>(name)
    .find(criteria.filter, criteria.fields)
    .toArray();
};

// tslint:disable-next-line:no-any
export const sendCollection = async (
  res: Response,
  name: string,
  criteria = { filter: {}, fields: {} },
) => {
  const data = await readCollection(name, criteria);
  return res.status(200).send(data);
};

export const deserializeId = <T extends { _id: string }>(obj: T) => {
  // tslint:disable-next-line:prefer-object-spread
  return Object.assign({}, obj, { id: new ObjectID(obj._id) });
};

export const removeKey = <T>(object: {}, key: string) => {
  const { [key]: value, ...rest } = object;
  return rest as T;
};
