import * as jwt from 'jsonwebtoken';
import * as moment from 'moment';
import { ObjectId } from 'mongodb';
import * as passport from 'passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { config } from 'src/app-config';
import { db } from 'src/mongo';

const secretOrKey = '2hOEP7F8H2@aX35t';

interface Payload {
  id: string;
  timestamp: number;
}

export const init = () => {
  passport.use(
    new Strategy(
      { jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(), secretOrKey },
      async (payload: Payload, callback) => {
        const tokenTime = moment.unix(payload.timestamp);
        const duration = moment.duration(moment().diff(tokenTime));
        if (duration.asSeconds() > config.tokenExpiration) {
          return callback(null, false);
        }

        const user = await db.collection('users').findOne({ _id: new ObjectId(payload.id) });
        if (!user) {
          return callback(null, false);
        }

        return callback(null, user);
      },
    ),
  );
};

export const getJwtToken = (id: string) => {
  const payload: Payload = {
    id,
    timestamp: moment().unix(),
  };

  return jwt.sign(payload, secretOrKey);
};
