import * as bodyParser from 'body-parser';
import * as fallback from 'connect-history-api-fallback';
import * as express from 'express';
import * as passport from 'passport';
import { config } from 'src/app-config';
import { corsControl } from 'src/middleware/corsControl';
import { errorHandler } from 'src/middleware/errorHandler';
// import { httpLogger } from 'src/middleware/httpLogger';
import { registerRoutes } from './routes';

let app: express.Application;

// app.options('*', cors());

export const start = () => {
  app = express();
  // app.use(httpLogger);
  app.use(corsControl);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded());
  app.use(passport.initialize());

  registerRoutes(app);

  app.use(fallback());
  app.use(express.static('static'));
  app.use(errorHandler);

  app.listen(config.core.port);
};
