import * as ejs from 'ejs';
import * as express from 'express';
import { getJwtToken } from 'src/authentication';
import { User } from 'src/models/db/User';
import { db } from 'src/mongo';
import loginView from 'src/views/login.ejs';
import {MongoClient } from 'mongodb';
import { config } from 'src/app-config';

export const usersRouter = express
  .Router()

  .get('/login', (_, res) => {
    res.status(200).send(ejs.render(loginView));
  })

  .post('/login', async (req, res) => {
    const user: User = await db
      .collection('users')
      .findOne({ username: req.body.username, password: req.body.password })!;

    if (!user) {
      res.send(401);
    }

    const token = getJwtToken(user._id.toHexString());
    res.send({ token });
  })
  .post('/registration', async (req, res) => {
    const newUser: User = req.body;
    MongoClient.connect(config.mongo.url, async (err, client) => {
      if (err) {
        throw err;
      }
      const dbo = client.db(config.mongo.dbName);
      dbo.collection('users').insertOne(newUser, async e => {
        if (e) {
          throw err;
        }
        // console.log("User registered in the database");
        client.close();
      });
    });

    res.json({ message: 'new user registered' });
  });
