import * as ejs from 'ejs';
import * as express from 'express';
import { config } from 'src/app-config';
import swaggerView from 'src/views/swagger.ejs';
import swaggerSpec from './swagger.yaml';

export const docsRouter = express
  .Router()

  .get('/', async (_, res) => {
    swaggerSpec.host = `${config.swaggerURL}:${config.core.swaggerPort}`;
    swaggerSpec.schemes = config.core.protocol;

    res.status(200).send(ejs.render(swaggerView, { spec: swaggerSpec }));
  });
