import { Application } from 'express';
import { authentication } from 'src/middleware/authentication';
import { docsRouter } from './docs';
import { usersRouter } from './users';

export const registerRoutes = (app: Application) => {
  const api = '/api';

  app.use(`${api}/users`, usersRouter);
  app.use(`${api}`, authentication, docsRouter);
};
