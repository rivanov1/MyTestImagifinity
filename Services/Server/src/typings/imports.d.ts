declare module "*.json" {
  const value: any;
  export default value;
}

declare module "*.yaml" {
  const value: any;
  export default value;
}

declare module "*.ejs" {
  const value: any;
  export default value;
}

declare module "*.html" {
  const value: any;
  export default value;
}