
declare module 'express-winston' {
  export let requestWhitelist: {
      push: (component: string) => {}
  };

  export let logger: (options: {
    colorize: boolean,
    expressFormat: boolean,
    ignoreRoute: () => boolean,
    meta: boolean,
    msg: string,
    transports: any[],
  }) => any[];
}
