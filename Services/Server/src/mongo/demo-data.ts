import { MongoClient, ObjectId } from 'mongodb';
import { config } from 'src/app-config';
import { User } from 'src/models/db/User';

MongoClient.connect(config.mongo.url, async (_, client): Promise<void> => {
  const db = client.db(config.mongo.dbName);

  const user: User = {
    _id: new ObjectId(),
    password: '123qwe',
    tenantId: 'cd119be8-da28-49b0-8035-7760ec8a0016',
    username: 'admin',
  };
  await db.collection('users').insertOne(user);
});
