import { Db, MongoClient } from 'mongodb';

export let db: Db;

export const connect = (url: string, dbName: string) => {
  MongoClient.connect(
    url,
    {
      autoReconnect: true,
      reconnectTries: Number.MAX_VALUE,
    },
    (_err, mongodClient): void => {
      db = mongodClient.db(dbName);
    },
  );
};
