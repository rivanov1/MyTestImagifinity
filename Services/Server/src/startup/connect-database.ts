import { config } from 'src/app-config';
import { connect } from 'src/mongo';

connect(config.mongo.url, config.mongo.dbName);
