const path = require('path');
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  output: {
    path: path.resolve(__dirname, 'build/prod'),
  },
  plugins: [
    new CleanWebpackPlugin(['build/prod']),
    new CopyWebpackPlugin([
      {
        from: 'src/app-config/config-template.prod.json',
        to: 'config.json',
      },
    ]),
  ],
});