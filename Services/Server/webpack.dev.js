const path = require('path');
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  output: {
    path: path.resolve(__dirname, 'build/dev'),
  },
  devtool: 'source-map',
  plugins: [
    new CleanWebpackPlugin(['build/dev']),
    new CopyWebpackPlugin([
      {
        from: 'src/app-config/config-template.dev.json',
        to: 'config.json',
      },
    ]),
  ],
});