const fs  = require('fs');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const lessToJs = require('less-vars-to-js');
const tsImportPluginFactory = require('ts-import-plugin');

const themeVariables = lessToJs(fs.readFileSync(
  path.resolve(__dirname, 'src/config/ant-theme-vars.less'), 'utf8'),
);

module.exports = {
  entry: {
    app: './src/index.ts',
  },
  output: {
    filename: '[name].js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    modules: [
      path.resolve(__dirname, './node_modules'),
      path.resolve(__dirname, '.'),
    ],
  },
  module: {
    rules: [
      { 
        test: /\.tsx?$/, 
        loader: 'ts-loader',
        options: {
          getCustomTransformers: () => ({
            before: [
              tsImportPluginFactory({
                libraryName: 'antd',
                libraryDirectory: 'lib',
                style: true,
              }),
            ],
          }),
        },
      },
      {
        test: /\.less$/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'less-loader',
            options: {
              modifyVars: themeVariables,
            },
          },
        ],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|jpg|gif|eot|svg|ttf|woff)$/,
        loader: 'url-loader',
        options: {
          limit: 262144,
        },
      },
    ],
  },
  target: 'web',
};
