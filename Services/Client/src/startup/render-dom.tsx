import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { AppContainer } from 'src/containers/AppContainer';

ReactDOM.render(<AppContainer />, document.getElementById('root') as HTMLElement);
