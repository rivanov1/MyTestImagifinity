import { applyMiddleware, compose, createStore, Store as ReduxStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer, State } from './reducers';

// tslint:disable-next-line:no-any
const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

export type Store = ReduxStore<State>;
