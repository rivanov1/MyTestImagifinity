import * as React from 'react';

import injectSheet, { JssProps } from 'react-jss';
import { Provider } from 'react-redux';
import { Dashboard } from 'src/components/pages/Dashboard';

import { Redirect, Route, Router, Switch } from 'react-router-dom';
import { RegistrationPage } from 'src/components/pages/RegistrationPage';

import { loadToken } from 'src/api/data';
import { LoginPage } from 'src/components/pages/LoginPage';
import { themeConfig } from 'src/config/theme.config';
import { store } from 'src/store';
import { history } from 'src/utils/history';
import { createStyles } from 'src/utils/jssUtils';

const styles = createStyles({
  app: {
    height: '100%',
    width: '100%',
  },
  content: {
    height: `calc(100% - ${themeConfig.topBar.height}px)`,
    overflow: 'auto',
    width: `calc(100% - ${themeConfig.sideBar.width}px)`,
  },
  sideBar: {
    display: 'flex',
    float: 'left',
    height: `calc(100% - ${themeConfig.topBar.height}px)`,
    width: themeConfig.sideBar.width,
  },
  topBar: {
    display: 'flex',
    height: themeConfig.topBar.height,
    width: '100%',
  },
});

interface Props {}

type P = Props & JssProps<typeof styles>;

// tslint:disable-next-line:no-any
const PrivateRoute = (routeProps: any) => {
  const { component: Component, ...rest } = routeProps;
  return (
    <Route
      {...rest}
      render={props =>
        loadToken() ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              // state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};
const AppContainerCont = (props: P) => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <div className={props.classes.app}>
          <div className={props.classes.topBar} />
          <Switch>
            <Route component={LoginPage} exact={true} path="/login" />
            <Route component={RegistrationPage} exact={true} path="/registration" />
            <Redirect exact={true} path="/" to="/dashboard" />
            <PrivateRoute
              component={() => (
                <>
                  <Route component={Dashboard} exact={true} path="/dashboard" />
                  <div className={props.classes.sideBar} />
                  <div className={props.classes.content}>
                    <Switch />
                  </div>
                </>
              )}
            />
          </Switch>
        </div>
      </Router>
    </Provider>
  );
};

export const AppContainer = injectSheet(styles)(AppContainerCont);
