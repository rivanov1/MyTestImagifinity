import { history } from 'src/utils/history';
import { Request, Response } from 'superagent';
import * as superagentDefaults from 'superagent-defaults';

const url =
  window.appConfig !== undefined && window.appConfig.apiUrl !== undefined
    ? window.appConfig.apiUrl
    : '/dsfksdljfsljdfkds';

export interface User {
  _id?: string;
  email: string;
  password: string;
  username: string;
}
export const clearToken = () => {
  try {
    localStorage.removeItem('token');
  } catch (err) {
    // Ignore write errors
  }
};

export const loadToken = () => {
  try {
    return localStorage.getItem('token');
  } catch (err) {
    return undefined;
  }
};

const saveToken = (token: string) => {
  try {
    localStorage.setItem('token', token);
  } catch (err) {
    // Ignore write errors
  }
};

const handleErrors = (req: Request) => {
  req.on('response', (res: Response) => {
    if (res.status === 401) {
      history.push('/login');
    }
  });
};

const agent = superagentDefaults();
agent.use(handleErrors);
const savedToken = loadToken();
if (savedToken) {
  agent.set('Authorization', `Bearer ${savedToken}`);
}

export const logIn = async (username: string, password: string) => {
  const response = await agent
    .post(`${url}/users/login`)
    .send({ username, password })
    .catch(e => {
      // Do nothing
      // tslint:disable-next-line:no-console
      console.log(e);
    });

  if (response) {
    const { token } = response.body;
    agent.set('Authorization', `Bearer ${token}`);
    saveToken(token);
    return true;
  }

  return false;
};
export const logOut = async () => {
  clearToken();
};
export const Registration = async (newUser: User) => {
  const response = await agent
    .post(`${url}/users/registration`)
    .send(newUser)
    .catch(e => {
      // Do nothing
      // tslint:disable-next-line:no-console
      console.log(e);
    });
  // const receivedMessage = response.body.message;
  if (response) {
    return true;
  } else {
    // console.log("Unsuccessful registration");
    return false;
  }
};
