"use strict";
/* tslint:disable */
/**
 * @license
 * Copyright 2017 Palantir Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
exports.__esModule = true;
var Lint = require("tslint");
var tsutils_1 = require("tsutils");
var ts = require("typescript");
var OPTION_IGNORE_CASE = "ignore-case";
var OPTION_SHORTHAND_FIRST = "shorthand-first";
var Rule = /** @class */ (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /* tslint:enable:object-literal-sort-keys */
    Rule.SHORTHAND_FAILURE_FACTORY = function (name) {
        return "Shorthand property '" + name + "' should come before normal properties";
    };
    Rule.ALHPA_FAILURE_FACTORY = function (name) {
        return "The key '" + name + "' is not sorted alphabetically";
    };
    Rule.prototype.apply = function (sourceFile) {
        return this.applyWithFunction(sourceFile, walk, parseOptions(this.ruleArguments));
    };
    /* tslint:disable:object-literal-sort-keys */
    Rule.metadata = {
        ruleName: "jsx-sort-props",
        description: "Requires props in JSX elements to be sorted alphabetically, and grouped as specified",
        optionsDescription: Lint.Utils.dedent(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n            Two arguments may be optionally provided:\n            * `\"", "\"`: Ignore case when comparing keys.\n            * `\"", "\"`: Enforces shorthand syntax appears first.             E.g. boolean attributes w/o a value: `<Component isError className=\"error\" />`\n        "], ["\n            Two arguments may be optionally provided:\n            * \\`\"", "\"\\`: Ignore case when comparing keys.\n            * \\`\"", "\"\\`: Enforces shorthand syntax appears first. \\\n            E.g. boolean attributes w/o a value: \\`<Component isError className=\"error\" />\\`\n        "])), OPTION_IGNORE_CASE, OPTION_SHORTHAND_FIRST),
        options: {
            type: "array",
            items: {
                "enum": [
                    OPTION_IGNORE_CASE,
                    OPTION_SHORTHAND_FIRST,
                ],
                type: "string"
            },
            additionalItems: false,
            minLength: 0,
            maxLength: 2
        },
        optionExamples: [
            "true",
            "[true, \"" + OPTION_IGNORE_CASE + "\"]",
            "[true, \"" + OPTION_IGNORE_CASE + "\", \"" + OPTION_SHORTHAND_FIRST + "\"]",
        ],
        type: "maintainability",
        typescriptOnly: false
    };
    return Rule;
}(Lint.Rules.AbstractRule));
exports.Rule = Rule;
function parseOptions(ruleArgs) {
    var options = {
        ignoreCase: ruleArgs.indexOf(OPTION_IGNORE_CASE) !== -1,
        shorthandFirst: ruleArgs.indexOf(OPTION_SHORTHAND_FIRST) !== -1
    };
    return options;
}
function walk(ctx) {
    var options = ctx.options, sourceFile = ctx.sourceFile;
    var ignoreCase = options.ignoreCase, shorthandFirst = options.shorthandFirst;
    function shouldCheckSyntax(currentProperty, lastProperty) {
        // Only check syntax if option is set, we have a property to check ,and they changed
        if (!shorthandFirst || lastProperty === undefined) {
            return false;
        }
        return !!currentProperty.initializer !== !!lastProperty.initializer;
    }
    function cb(node) {
        // Only check object literals with at least one key
        if (!tsutils_1.isJsxOpeningLikeElement(node)) {
            return ts.forEachChild(node, cb);
        }
        var properties = node.attributes.properties;
        if (properties.length <= 1) {
            return ts.forEachChild(node, cb);
        }
        var lastKey;
        var lastProperty;
        outer: for (var _i = 0, properties_1 = properties; _i < properties_1.length; _i++) {
            var property = properties_1[_i];
            // Only evaluate properties that apply
            switch (property.kind) {
                // Restart ordering after spread assignments
                case ts.SyntaxKind.JsxSpreadAttribute:
                    lastKey = undefined;
                    lastProperty = undefined;
                    break;
                case ts.SyntaxKind.JsxAttribute:
                    var propName = property.name;
                    var propText = propName.text;
                    var key = ignoreCase ? propText.toLowerCase() : propText;
                    if (shouldCheckSyntax(property, lastProperty)) {
                        // Syntax changed and it's shorthand now, so we were not previously
                        if (property.initializer === undefined) {
                            ctx.addFailureAtNode(propName, Rule.SHORTHAND_FAILURE_FACTORY(propText));
                            break outer;
                        }
                        // Reset the alpha keys to re-start alpha sorting by syntax
                        lastKey = key;
                        lastProperty = property;
                        break;
                    }
                    // comparison with undefined is expected
                    if (lastKey > key) {
                        ctx.addFailureAtNode(propName, Rule.ALHPA_FAILURE_FACTORY(propText));
                        break outer;
                    }
                    lastKey = key;
                    lastProperty = property;
            }
        }
        return ts.forEachChild(node, cb);
    }
    return ts.forEachChild(sourceFile, cb);
}
var templateObject_1;
