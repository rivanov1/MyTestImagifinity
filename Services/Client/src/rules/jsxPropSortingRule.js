"use strict";
/* tslint:disable */
/**
 * @license
 * Copyright 2016 Palantir Technologies, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var ts = require("typescript");
var Lint = require("tslint");
var Rule = /** @class */ (function (_super) {
    __extends(Rule, _super);
    function Rule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Rule.prototype.apply = function (sourceFile) {
        var walker = new JsxPropSortingWalker(sourceFile, this.getOptions());
        return this.applyWithWalker(walker);
    };
    Rule.FAILURE_STRING = "Prop names are not alphabetized";
    return Rule;
}(Lint.Rules.AbstractRule));
exports.Rule = Rule;
var JsxPropSortingWalker = /** @class */ (function (_super) {
    __extends(JsxPropSortingWalker, _super);
    function JsxPropSortingWalker() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    JsxPropSortingWalker.prototype.visitInterfaceDeclaration = function (node) {
        var _this = this;
        var prevChildName = "";
        var currChildName = "";
        ts.forEachChild(node, function (child) {
            if (child.kind === ts.SyntaxKind.PropertySignature) {
                currChildName = child.getText();
                if (currChildName.localeCompare(prevChildName) > 0) {
                    prevChildName = currChildName;
                }
                else {
                    _this.addFailure(_this.createFailure(child.getStart(), child.getWidth(), Rule.FAILURE_STRING));
                }
            }
        });
        _super.prototype.visitInterfaceDeclaration.call(this, node);
    };
    return JsxPropSortingWalker;
}(Lint.RuleWalker));
