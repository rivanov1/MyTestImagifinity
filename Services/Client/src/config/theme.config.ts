export const themeConfig = {
  screens: {
    background: '#fff',
    paddingBottom: 22,
    paddingLeft: 31,
    paddingRight: 31,
    paddingTop: 22,
  },
  sideBar: {
    background: '#d7edca',
    selectedColor: '#72bf44',
    width: 48,
  },
  topBar: {
    background: '#d7edca',
    height: 43,
    logo: {
      background: '#d7edca',
    },
  },
};
