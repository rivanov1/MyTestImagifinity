declare module 'superagent-defaults' {
  import { Plugin, Request, SuperAgent, SuperAgentRequest } from 'superagent';

  let getAgent: () => SuperAgent<SuperAgentRequest> & {
    set(field: string, val: string): Request;
    use(fn: Plugin): Request;
  };
  export = getAgent
}
