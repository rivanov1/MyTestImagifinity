// Ensure this is treated as a module.
export {};

declare global {
  interface Window {
    appConfig?: {
      apiUrl?: string;
    };
  }
}

declare function coalesce<T, K extends T>(value: T | undefined, defaultValue: K) : T;

declare function removeKey<T>(object: {}, key: string): T;
