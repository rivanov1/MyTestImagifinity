import * as React from 'react';

declare module 'react-redux' {
  export interface InferableComponentEnhancerWithProps<TInjectedProps, TNeedsProps> {
    <P extends TInjectedProps>(component: Component<P>): ComponentClass<
      Omit<P, keyof TInjectedProps> & TNeedsProps
    > & {
      WrappedComponent: Component<P>;
    };

    allProps: TInjectedProps & TNeedsProps;
  }
}