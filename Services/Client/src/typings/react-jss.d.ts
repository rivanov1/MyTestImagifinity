declare module 'react-jss' {
  type CSSBorder = string;
  
  type CSSColor = string;

  type CSSLength = number | string;

  type CSSPercentage = string;

  type CSSSize = number | string;

  type CSSProperty<T> = CSSPropertyFunction<T> | T;

  type CSSPropertyFunction<T> = (props: any) => T;

  export interface CSSProperties {
    alignItems?: CSSProperty<"flex-start" | "flex-end" | "center" | "baseline" | "stretch">;

    alignSelf?: CSSProperty<"auto" | "flex-start" | "flex-end" | "center" | "baseline" | "stretch">;

    backgroundColor?: CSSProperty<CSSColor>;

    border?: CSSProperty<CSSBorder>;

    borderBottom?: CSSProperty<CSSBorder>;

    borderColor?: CSSProperty<CSSColor>;

    borderRadius?: CSSProperty<CSSLength>;

    borderRight?: CSSProperty<CSSBorder>;

    borderTop?: CSSProperty<CSSBorder>;

    color?: CSSProperty<CSSColor>;

    cursor?: CSSProperty<"pointer">;

    display?: CSSProperty<"flex" | "inline-flex">;

    flexBasis?: CSSProperty<CSSSize>;

    flexDirection?: CSSProperty<"row" | "row-reverse" | "column" | "column-reverse">;

    flexGrow?: CSSProperty<number>;

    flexShrink?: CSSProperty<number>;

    float?: CSSProperty<"left" | "right">;

    fontFamily?: CSSProperty<string>;

    fontSize?: CSSProperty<CSSLength | CSSPercentage |
      "xx-small" | "x-small" | "small" | "medium" | "large" | "x-large" | "xx-large" |
      "larger" | "smaller">;
    
    fontWeight?: CSSProperty<"normal" | "bold" | "bolder" | "lighter" |
      100 | 200 | 300 | 400 | 500 | 600 | 700 | 800 | 900>;
    
    height?: CSSProperty<CSSSize>;
    
    justifyContent?: CSSProperty<"flex-start" | "flex-end" | "center" |
      "space-between" | "space-around" | "space-evenly">;

    left?: CSSProperty<CSSSize>;

    marginBottom?: CSSProperty<CSSSize>;
  
    marginLeft?: CSSProperty<CSSSize>;
  
    marginRight?: CSSProperty<CSSSize>;
  
    marginTop?: CSSProperty<CSSSize>;

    maxWidth?: CSSProperty<CSSSize>;
  
    minHeight?: CSSProperty<CSSSize>;

    minWidth?: CSSProperty<CSSSize>;

    overflow?: CSSProperty<"auto" | "hidden" | "scroll" | "visible">;

    paddingBottom?: CSSProperty<CSSSize>;

    paddingLeft?: CSSProperty<CSSSize>;
    
    paddingRight?: CSSProperty<CSSSize>;
    
    paddingTop?: CSSProperty<CSSSize>;
    
    position?: CSSProperty<"absolute" | "relative">;
    
    textAlign?: CSSProperty<"left" | "right" | "center" | "justifyny">;
  
    top?: CSSProperty<CSSSize>;

    userSelect?: CSSProperty<"none">,

    width?: CSSProperty<CSSSize>;
  }

  export type Styles<T extends string> = { [key in T]: CSSProperties };

  export type Classes<T extends Styles<keyof T>> = { [key in keyof T]: string };
  
  export interface JssProps<T extends Styles<keyof T>> {
    classes: Classes<T>
  }

  export default function injectSheet<T extends Styles<keyof T>>(
    styles: T
  ): <P>(component: React.ComponentType<{ classes: Classes<T> } & P>) =>
      React.ComponentClass<P>
}