import * as React from 'react';

export const Dummy = (): React.SFC | undefined => undefined;

declare module 'react-leaflet' {
  export interface PopupProps {
    onClose?: () => void;
    onOpen?: () => void;
  }
}
