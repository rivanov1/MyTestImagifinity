import { Button, Form } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import * as React from 'react';
import injectSheet, { JssProps } from 'react-jss';
import { logOut } from 'src/api/data';
import { Screen } from 'src/components/shared';
import { createStyles } from 'src/utils/jssUtils';
const FormItem = Form.Item;
import { RouteComponentProps, withRouter } from 'react-router';

interface LoginScreenProps {
  test: string;
}
const styles = createStyles({
  SignOutFormButton: {
    width: '100%',
  },
});

type Props = LoginScreenProps &
  JssProps<typeof styles> &
  FormComponentProps &
  RouteComponentProps<LoginScreenProps>;
class DashboardComponent extends React.Component<Props> {
  private handleLogOut = async (e: React.FormEvent<HTMLFormElement>) => {
    logOut();
    this.props.history.push('/login');
  };
  public render() {
    const props = this.props;
    return (
      <Screen>
        <Form>
          <FormItem>
            <Button
              className={props.classes.SignOutFormButton}
              type="primary"
              onClick={this.handleLogOut}
            >
              Sign Out
            </Button>
          </FormItem>
        </Form>
      </Screen>
    );
  }
}
export const Dashboard = withRouter(Form.create()(injectSheet(styles)(DashboardComponent)));
