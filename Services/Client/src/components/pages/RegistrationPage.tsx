import { Button, Form, Icon, Input, notification } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import * as React from 'react';
import injectSheet, { JssProps } from 'react-jss';
import { Registration } from 'src/api/data';
import { Screen } from 'src/components/shared';
import { createStyles } from 'src/utils/jssUtils';
const FormItem = Form.Item;
import { RouteComponentProps, withRouter } from 'react-router';

interface LoginScreenProps {
  test: string;
}

const styles = createStyles({
  registrationForm: {
    maxWidth: 800,
  },
  registrationFormButton: {
    width: '100%',
  },
  registrationFormForgot: {
    float: 'right',
  },
  registrationScreen: {
    alignItems: 'center',
    display: 'flex',
    flexGrow: 1,
    justifyContent: 'center',
  },
});

type Props = LoginScreenProps &
  JssProps<typeof styles> &
  FormComponentProps &
  RouteComponentProps<LoginScreenProps>;

class RegistrationPageComp extends React.Component<Props> {
  private handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    this.props.form.validateFields(
      async (
        err,
        values: {
          email: string;
          username: string;
          password: string;
        },
      ) => {
        if (!err) {
          if (
            await Registration({
              email: values.email,
              password: values.password,
              username: values.username,
            })
          ) {
            this.props.history.push('/');
          } else {
            this.props.form.resetFields();
            notification.error({
              description: 'User or password are incorrect',
              message: 'Error',
            });
          }
        }
      },
    );
  };

  public render() {
    const props = this.props;
    const { getFieldDecorator } = props.form;
    return (
      <Screen>
        <div className={props.classes.registrationScreen}>
          <Form className={props.classes.registrationForm} onSubmit={this.handleSubmit}>
            <FormItem>
              Username:
              {getFieldDecorator('username', {
                rules: [{ required: true, message: 'Please choose your username!' }],
              })(
                <Input
                  placeholder="Username"
                  prefix={<Icon style={{ color: 'rgba(0,0,0,.25)' }} type="user" />}
                />,
              )}
            </FormItem>
            <FormItem>
              Password:
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please input your Password!' }],
              })(
                <Input
                  placeholder="Password"
                  prefix={<Icon style={{ color: 'rgba(0,0,0,.25)' }} type="lock" />}
                  type="password"
                />,
              )}
            </FormItem>
            <FormItem>
              Confirm password:
              {getFieldDecorator('confirmPassword', {
                rules: [{ required: true, message: 'Confirm your Password!' }],
              })(
                <Input
                  placeholder="Password"
                  prefix={<Icon style={{ color: 'rgba(0,0,0,.25)' }} type="lock" />}
                  type="password"
                />,
              )}
            </FormItem>
            <FormItem>
              Email:
              {getFieldDecorator('email', {
                rules: [{ required: true, message: 'Enter your email!' }],
              })(
                <Input
                  placeholder="Email"
                  prefix={<Icon style={{ color: 'rgba(0,0,0,.25)' }} type="lock" />}
                  type="email"
                />,
              )}
            </FormItem>
            <FormItem>
              <Button
                className={props.classes.registrationFormButton}
                htmlType="submit"
                type="primary"
              >
                Register
              </Button>
            </FormItem>
          </Form>
        </div>
      </Screen>
    );
  }
}
export const RegistrationPage = withRouter(
  Form.create()(injectSheet(styles)(RegistrationPageComp)),
);
