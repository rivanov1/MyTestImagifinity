import { Button, Checkbox, Form, Icon, Input, notification } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import * as React from 'react';
import injectSheet, { JssProps } from 'react-jss';
import { logIn } from 'src/api/data';
import { Screen } from 'src/components/shared';
import { createStyles } from 'src/utils/jssUtils';
const FormItem = Form.Item;
import { RouteComponentProps, withRouter } from 'react-router';

interface LoginScreenProps {
  test: string;
}

const styles = createStyles({
  loginForm: {
    maxWidth: 300,
  },
  loginFormButton: {
    width: '100%',
  },
  loginFormForgot: {
    float: 'right',
  },
  loginScreen: {
    alignItems: 'center',
    display: 'flex',
    flexGrow: 1,
    justifyContent: 'center',
  },
});

type Props = LoginScreenProps &
  JssProps<typeof styles> &
  FormComponentProps &
  RouteComponentProps<LoginScreenProps>;

class LoginPageComp extends React.Component<Props> {
  private handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        if (await logIn(values.userName, values.password)) {
          this.props.history.push('/');
        } else {
          this.props.form.resetFields();
          notification.error({
            description: 'User or password are incorrect',
            message: 'Error',
          });
        }
      }
    });
  };

  public render() {
    const props = this.props;
    const { getFieldDecorator } = props.form;
    return (
      <Screen>
        <div className={props.classes.loginScreen}>
          <Form className={props.classes.loginForm} onSubmit={this.handleSubmit}>
            <FormItem>
              {getFieldDecorator('userName', {
                rules: [{ required: true, message: 'Please input your username!' }],
              })(
                <Input
                  placeholder="Username"
                  prefix={<Icon style={{ color: 'rgba(0,0,0,.25)' }} type="user" />}
                />,
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please input your Password!' }],
              })(
                <Input
                  placeholder="Password"
                  prefix={<Icon style={{ color: 'rgba(0,0,0,.25)' }} type="lock" />}
                  type="password"
                />,
              )} 
            </FormItem>
            <FormItem>
              {getFieldDecorator('remember', {
                initialValue: true,
                valuePropName: 'checked',
              })(<Checkbox>Remember me</Checkbox>)}
              <a className={props.classes.loginFormForgot} href="">
                Forgot password
              </a>
              <Button className={props.classes.loginFormButton} htmlType="submit" type="primary">
                Log in
              </Button>
              Or <a href="/registration">register now!</a>
            </FormItem>
          </Form>
        </div>
      </Screen>
    );
  }
}

export const LoginPage = withRouter(Form.create()(injectSheet(styles)(LoginPageComp)));
