import * as classNames from 'classnames';
import * as React from 'react';
import injectSheet, { Classes } from 'react-jss';
import { RouteComponentProps, withRouter } from 'react-router';
import dashboardImage from 'src/assets/img/sidebar/dashboard.svg';
import historyImage from 'src/assets/img/sidebar/history.svg';
import inventoryImage from 'src/assets/img/sidebar/inventory.svg';
import regionsDeposImage from 'src/assets/img/sidebar/regions-depos.svg';
import routingImage from 'src/assets/img/sidebar/routing.svg';
import settingsImage from 'src/assets/img/sidebar/settings.svg';
import typesImage from 'src/assets/img/sidebar/types.svg';
import { themeConfig } from 'src/config/theme.config';
import { createStyles } from 'src/utils/jssUtils';

const styles = createStyles({
  button: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
    minHeight: themeConfig.sideBar.width,
  },
  image: {
    userSelect: 'none',
  },
  selectedButton: {
    backgroundColor: themeConfig.sideBar.selectedColor,
  },
  settingsButton: {
    marginTop: 'auto',
  },
  sideBar: {
    backgroundColor: themeConfig.sideBar.background,
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
  },
});

interface Props {
  classes: Classes<typeof styles>;
}

class SiderBarComp extends React.Component<Props & RouteComponentProps<Props>> {
  private handleClick = (event: React.SyntheticEvent<HTMLDivElement>, key: string) => {
    this.props.history.push(`/${key}`);
  };

  public render() {
    const classes = this.props.classes;
    const selectedKey = this.props.history.location.pathname.substring(1);
    return (
      <div className={classes.sideBar}>
        {[
          { key: 'dashboard', image: dashboardImage },
          { key: 'routing', image: routingImage },
          { key: 'history', image: historyImage },
          { key: 'inventory', image: inventoryImage },
          { key: 'regions-depos', image: regionsDeposImage },
          { key: 'types', image: typesImage },
          { key: 'settings', image: settingsImage },
        ].map(x => (
          <div
            className={classNames(
              classes.button,
              x.key === 'settings' ? classes.settingsButton : '',
              selectedKey === x.key ? classes.selectedButton : '',
            )}
            key={x.key}
            onClick={(e: React.SyntheticEvent<HTMLDivElement>) => this.handleClick(e, x.key)}
          >
            <img className={this.props.classes.image} src={x.image} />
          </div>
        ))}
      </div>
    );
  }
}

export const SideBar = withRouter(injectSheet(styles)(SiderBarComp));
