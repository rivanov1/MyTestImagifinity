import * as React from 'react';
import injectSheet, { Classes } from 'react-jss';
import { RouteComponentProps, withRouter } from 'react-router';
import { clearToken, loadToken } from 'src/api/data';
import logoImage from 'src/assets/img/logo.png';
import { themeConfig } from 'src/config/theme.config';
import { createStyles } from 'src/utils/jssUtils';

const styles = createStyles({
  logInOut: {
    marginRight: 0,
  },
  logo: {
    alignItems: 'center',
    alignSelf: 'stretch',
    backgroundColor: themeConfig.topBar.logo.background,
    display: 'flex',
    flexBasis: themeConfig.sideBar.width,
    justifyContent: 'center',
  },
  screenLabel: {
    color: '#72bf44',
    fontFamily: 'Helvetica',
    fontSize: 20,
    fontWeight: 300,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  topBar: {
    alignItems: 'center',
    backgroundColor: themeConfig.topBar.background,
    borderBottom: 'solid 1px #72bf44',
    display: 'flex',
    flexGrow: 1,
    paddingRight: 15,
  },
});

interface Props {}

interface StylingProps {
  classes: Classes<typeof styles>;
}

type P = Props & StylingProps & RouteComponentProps<Props>;

const TopBarComp = (props: P) => {
  let header: string;
  switch (props.history.location.pathname) {
    case '/dashboard':
      header = 'Dashboard';
      break;
    case '/routing':
      header = 'Routing';
      break;
    case '/history':
      header = 'History';
      break;
    case '/inventory':
      header = 'Resources - Inventory';
      break;
    case '/regions-depos':
      header = 'Resources - Regions & Depos';
      break;
    case '/types':
      header = 'Resources - Types';
      break;
    case '/settings':
      header = 'Settings';
      break;
    default:
      header = '';
      break;
  }
  return (
    <div className={props.classes.topBar}>
      <div className={props.classes.logo}>
        <img src={logoImage} />
      </div>
      <div className={props.classes.screenLabel}>{header}</div>
      {loadToken() ? (
        <div className={props.classes.logInOut}>
          <a
            onClick={() => {
              clearToken();
              props.history.push('/');
            }}
          >
            Log out
          </a>
        </div>
      ) : (
        undefined
      )}
    </div>
  );
};

export const TopBar = withRouter(injectSheet(styles)(TopBarComp));
