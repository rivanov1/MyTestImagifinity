import { Tabs } from 'antd';
import * as classNames from 'classnames';
import * as React from 'react';
import injectSheet, { JssProps } from 'react-jss';
import { createStyles } from 'src/utils/jssUtils';

interface TabViewProps {
  children: JSX.Element | JSX.Element[];
  className?: string;
}

const styles = createStyles({
  tabs: {
    alignItems: 'stretch',
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
  },
});

type Props = TabViewProps & JssProps<typeof styles>;

const TabViewComp: React.SFC<Props> = (props: Props) => {
  return (
    <Tabs
      className={classNames(props.classes.tabs, props.className)}
      tabBarStyle={{
        color: '#72bf44',
        fontFamily: 'Helvetica',
      }}
    >
      {props.children}
    </Tabs>
  );
};

TabViewComp.defaultProps = {
  className: '',
};

export const TabView = injectSheet(styles)(TabViewComp);
