import * as React from 'react';
import injectSheet, { JssProps } from 'react-jss';
import { AutoSizer } from 'react-virtualized';
import { createStyles } from 'src/utils/jssUtils';
import { DecoratedTableView, DecoratedTableViewProps } from './DecoratedTableView';

type AutosizeTableViewProps<T> = DecoratedTableViewProps<T>;

const styles = createStyles({
  container: {
    flexGrow: 1,
  },
});

type Props<T> = AutosizeTableViewProps<T> & JssProps<typeof styles>;

const AutosizeTableViewComp = <T extends {}>(props: Props<T>) => {
  return (
    <div className={props.classes.container}>
      <AutoSizer>
        {size => (
          <DecoratedTableView
            {...props}
            scroll={{ y: size.height - 36 }}
            // tslint:disable-next-line:jsx-ban-props
            style={{ width: size.width }}
          />
        )}
      </AutoSizer>
    </div>
  );
};

export const AutosizeTableView = injectSheet(styles)(AutosizeTableViewComp);
