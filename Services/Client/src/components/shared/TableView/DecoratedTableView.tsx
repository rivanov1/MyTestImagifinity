import { Table } from 'antd';
import { TableProps } from 'antd/lib/table';
import * as React from 'react';
import injectSheet, { JssProps } from 'react-jss';
import { createStyles } from 'src/utils/jssUtils';

export type DecoratedTableViewProps<T> = Omit<TableProps<T>, 'pagination' | 'size'>;

const styles = createStyles({
  title: {
    float: 'left',
    fontWeight: 600,
  },
});

type Diff<T extends string, U extends string> = ({ [P in T]: P } &
  { [P in U]: never } & { [x: string]: never })[T];
type Omit<T, K extends keyof T> = Pick<T, Diff<keyof T, K>>;

type Props<T> = DecoratedTableViewProps<T> & JssProps<typeof styles>;

const DecoratedTableViewComp = <T extends {}>(props: Props<T>) => {
  const columns = props.columns!.map(x => ({
    ...x,
    title: <div className={props.classes.title}>{x.title}</div>,
  }));

  // tslint:disable-next-line:jsx-ban-elements
  return <Table {...props} columns={columns} pagination={false} size="small" />;
};

export const DecoratedTableView = injectSheet(styles)(DecoratedTableViewComp);
