import { Tabs } from 'antd';
import { TabPaneProps } from 'antd/lib/tabs';
import * as classNames from 'classnames';
import * as React from 'react';
import injectSheet, { JssProps } from 'react-jss';
import { createStyles } from 'src/utils/jssUtils';
const TabPane = Tabs.TabPane;

type TabPaneViewProps = TabPaneProps;

const styles = createStyles({
  tabs: {
    alignItems: 'stretch',
    display: 'flex',
    flexGrow: 1,
  },
});

type Props = TabPaneViewProps & JssProps<typeof styles>;

const TabPaneViewComp: React.SFC<Props> = (props: Props) => {
  return (
    // tslint:disable-next-line:jsx-ban-elements
    <TabPane {...props} className={classNames(props.classes.tabs, props.className)} />
  );
};

TabPaneViewComp.defaultProps = {
  className: '',
};

export const TabPaneView = injectSheet(styles)(TabPaneViewComp);
