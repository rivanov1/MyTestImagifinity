import * as classNames from 'classnames';
import * as React from 'react';
import injectSheet, { Classes } from 'react-jss';
import { themeConfig } from 'src/config/theme.config';
import { createStyles } from 'src/utils/jssUtils';

const styles = createStyles({
  screen: {
    backgroundColor: themeConfig.screens.background,
    display: 'inline-flex',
    minHeight: '100%',
    minWidth: '100%',
    paddingBottom: themeConfig.screens.paddingBottom,
    paddingLeft: themeConfig.screens.paddingLeft,
    paddingRight: themeConfig.screens.paddingRight,
    paddingTop: themeConfig.screens.paddingTop,
  },
});

interface Props {
  children?: JSX.Element | JSX.Element[];
  className?: string;
}

interface StyledProps {
  classes: Classes<typeof styles>;
}

type P = Props & StyledProps;

const ScreenComp: React.StatelessComponent<P> = (props: P) => {
  return <div className={classNames(props.classes.screen, props.className)}>{props.children}</div>;
};

ScreenComp.defaultProps = {
  children: undefined,
  className: '',
};

export const Screen = injectSheet(styles)(ScreenComp);
