import * as React from 'react';
import injectSheet, { JssProps } from 'react-jss';
import { Screen } from 'src/components/shared';
import { createStyles } from 'src/utils/jssUtils';

interface PaneProps {
  leftCol: JSX.Element;
  rightCol: JSX.Element;
}

const styles = createStyles({
  leftCol: {
    alignItems: 'stretch',
    display: 'flex',
    flexBasis: 0.5,
    flexDirection: 'column',
    flexGrow: 1,
    flexShrink: 0,
    minWidth: 600,
    paddingRight: 40,
  },
  rightCol: {
    display: 'flex',
    flexBasis: 0.5,
    flexDirection: 'column',
    flexGrow: 1,
  },
});

type Props = PaneProps & JssProps<typeof styles>;

const PaneComp = (props: Props) => {
  return (
    <Screen>
      <div className={props.classes.leftCol}>{props.leftCol}</div>
      <div className={props.classes.rightCol}>{props.rightCol}</div>
    </Screen>
  );
};

export const Pane = injectSheet(styles)(PaneComp);
