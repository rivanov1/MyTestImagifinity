import * as React from 'react';
import injectSheet, { JssProps } from 'react-jss';
import { createStyles } from 'src/utils/jssUtils';

interface HeaderProps {
  readonly text: string;
}

const styles = createStyles({
  header: {
    color: '#72bf44',
    fontFamily: 'Helvetica',
    fontSize: 18,
    marginBottom: 15,
  },
});

type Props = HeaderProps & JssProps<typeof styles>;

const HeaderComp = (props: Props) => {
  return <div className={props.classes.header}>{props.text}</div>;
};

export const Header = injectSheet(styles)(HeaderComp);
