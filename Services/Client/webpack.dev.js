const path = require('path');
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  output: {
    path: path.resolve(__dirname, 'build/dev'),
  },
  devtool: 'source-map',
  devServer: {
    contentBase:  path.resolve(__dirname, 'build/dev'),
    compress: true,
    port: 8000,
    historyApiFallback: true,
    watchOptions: {
      ignored: /node_modules/
    },
  },
  plugins: [
    new CleanWebpackPlugin(['build/dev']),
    new CopyWebpackPlugin([
      {
        from: 'src/assets/index-template.dev.html',
        to: 'index.html',
      },
    ]),
  ],
});